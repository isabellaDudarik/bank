public class Generator {
    public static Bank[] generatecurrency() {
        Bank bank[] = new Bank[3];
        bank[0] = new Bank();

        bank[0].banksname = "Privatbank";
        bank[0].array = new Currency[3];
        bank[0].array[0] = new Currency("USD", 26.8000f, 27.2000f);
        bank[0].array[1] = new Currency("EUR", 30.0000f, 30.6000f);
        bank[0].array[2] = new Currency("RUB", 0.3900f, 0.4250f);


        bank[1] = new Bank();
        bank[1].banksname = "Оschadbank";
        bank[1].array = new Currency[3];
        bank[1].array[0] = new Currency("USD", 26.8000f, 27.2000f);
        bank[1].array[1] = new Currency("EUR", 29.9500f, 30.6500f);
        bank[1].array[2] = new Currency("RUB", 0.2600f, 0.4280f);


        bank[2] = new Bank();
        bank[2].banksname = "Alphabank";
        bank[2].array = new Currency[3];
        bank[2].array[0] = new Currency("USD", 26.8500f, 27.2000f);
        bank[2].array[1] = new Currency("EUR", 30.1000f, 30.7000f);
        bank[2].array[2] = new Currency("RUB", 0.4100f, 0.4320f);

        return bank;
    }
}