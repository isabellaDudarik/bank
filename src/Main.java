import java.util.Locale;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Enter the amount of money that you want to change: ");
        int amount = Integer.parseInt(scan.nextLine());

        System.out.println("Enter the currency to convert (USD or EUR or RUB): ");
        String currency = scan.nextLine();

        System.out.println("Enter the procedure to convert (buy or sell): ");
        String procedure = scan.nextLine();

        System.out.println("Enter the bank for conversion (Privatbank or Оschadbank or Alphabank): ");
        String bank1 = scan.nextLine();
        Converter.currencyConversion(amount,currency,procedure, bank1);

    }
}
