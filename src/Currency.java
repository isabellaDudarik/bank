public class Currency {
    public String namecurrency;
    public float buy;
    public float sell;

    public Currency(String namecurrency, float buy, float sell) {
        this.namecurrency = namecurrency;
        this.buy = buy;
        this.sell = sell;
    }
}
